from flask import Flask, Response, render_template, current_app
from flask_bootstrap import Bootstrap
import time

def create_app(static_folder='static', static_url_path='/static'):
    from coinbase import CoinbaseAccount
    
    app = Flask(__name__)
    app.config.from_pyfile('config_default.py')
    Bootstrap(app)

    @app.route('/')
    def index():
        account = CoinbaseAccount(api_key=current_app.config.get('CB_API_KEY'))
        account_value = account.sell_price(qty=account.balance)
        buy_price = account.buy_price()
        return render_template('index.html', account_value=account_value, buy_price=buy_price)

    @app.route('/fetch-buy-price')
    def stream_buy_price():
        account = CoinbaseAccount(api_key=current_app.config.get('CB_API_KEY'))
        def get():
            yield 'data: %s\n\n' % account.buy_price()
            time.sleep(7)

        return Response(get(), content_type='text/event-stream')

    return app
